<?PHP

 if (!preg_match('/chris\.smith/i', $_SERVER['HTTP_HOST'])) {
  header('Location: http://chris.smith.name/');
  exit();
 }

 $xml = new SimpleXMLElement(file_get_contents('foaf.xml'));

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <title>Personal homepage of Chris Smith</title>
   <link rel="openid.server" href="http://chris.smith.name/openid/">
   <link rel="openid.delegate" href="http://chris.smith.name/openid/">
   <link rel="meta" type="application/rdf+xml" title="FOAF" href="http://chris.smith.name/foaf.xml">
<?PHP

 foreach ($xml->xpath('//foaf:OnlineAccount') as $account) {
  $label = $account->children('http://www.w3.org/2000/01/rdf-schema#')->label;
  $info  = $account->children('http://xmlns.com/foaf/0.1/')->accountProfilePage->attributes('http://www.w3.org/1999/02/22-rdf-syntax-ns#');
  $info  = (string) $info['resource'];
  echo '   <link rel="me" type="text/html" title="', htmlentities($label), '" href="', htmlentities($info), '">', "\n";
 }


?>
   <style type="text/css">
   body {
    margin: 50px 10%;
    font-family: "DejaVu Sans", sans-serif;
   }

   h1 {
    border-bottom: 3px double #aaa;
    color: #aaa;
   }

   div#footer {
    clear: both;
    padding-top: 20px;
    border-bottom: 3px double #aaa;
   }

   h1 span {
    padding-left: 3px;
    color: #000;
   }

   div.left {
    float: left;
    width: 48%;
    clear: both;
   }

   div.right {
    float: right;
    width: 48%;
   }

   a img {
    border: 0px;
   }

   dd {
    margin-bottom: 10px;
   }

   p, dd, dt {
    font-family: sans-serif;
    text-align: justify;
    line-height: 150%;
   }
  </style>
 </head>
 <body>
  <h1>Personal homepage of <span>Chris Smith</span></h1>
  <div class="left">
   <h2>Me</h2>
   <p>
    I'm a Linux-using, open-source-advocating software developer.
    I'm interested in application, computer and network security,
    data mining, and usability. I enjoy reading (about an 80:20
    split between non-fiction and sci-fi).
   </p>
   <p>
    Development-wise, my current language of choice is Java, but
    I also widely use PHP.  I can use HTML, CSS, XML, JavaScript,
    and SQL.  I used to use (and probably still could if needed)
    Delphi (object pascal), Visual Basic and Tcl. I have a basic
    knowledge of C, Ruby, Python and Perl.
   </p>
  </div>
  <div class="right">
   <h2>Projects</h2>
   <p>
    Some of the recent projects I've been working on include:
   </p>
   <dl>
<?PHP /*    <dt><a href="/projects/japoker">JaPoker</a></dt>
    <dd>A Java Poker game</dd> */ ?>
    <dt><a href="/projects/jircstats">JIRCStats</a></dt>
    <dd>An IRC log file parser and statistics generator</dd>
    <dt><a href="/projects/dmdirc">DMDirc</a></dt>
    <dd>An open-source, cross-platform IRC client</dd>
   </dl>
   <p>
    A complete list of projects is available on my
    <a href="/portfolio/">portfolio</a> page.
   </p>
  </div>
  <div class="left">
   <h2>This site</h2>
   <p>
    There used to be a reasonably large site here, instead of
    this one page.  I rarely updated it, and I can't imagine anyone
    was that interested in the content, so instead of having a site
    that I felt guilty for not updating, I scrapped it and
    simplified the content down to this page.
   </p>
  </div>
  <div class="right">
   <h2>Contact</h2>
   <p>
    I can be contacted by e-mail at
    <a href="mailto:chris87@gmail.com">chris87@gmail.com</a> (I get
    so much spam it's not worth me trying to obfuscate it).
    Alternatively, you can contact me on
    <a href="irc://irc.quakenet.org/mdbot">irc</a> (my nickname is 'MD87'),
    Google Talk or MSN (both using my e-mail address above).
   </p>
  </div>
  <div id="footer"></div>
 </body>
</html>
