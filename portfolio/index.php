<?PHP

 if (!preg_match('/chris\.smith/i', $_SERVER['HTTP_HOST'])) {
  header('Location: http://chris.smith.name/portfolio/');
  exit();
 }


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <title>Portfolio of Chris Smith</title>
   <style type="text/css">
   body {
    margin: 50px 10%;
   }

   h1 {
    border-bottom: 3px double #aaa;
    color: #aaa;
   }

   div#footer {
    clear: both;
    padding-top: 20px;
    border-bottom: 3px double #aaa;
    margin-bottom: 6px;
   }

   h1 span {
    padding-left: 3px;
    color: #000;
   }

   p {
    line-height: 150%;
   }

   a.portfolio {
    display: block;
    float: left;
    margin: 5px 10px;
    min-height: 300px;
    width: 300px;
    height: 300px;
    color: black;
    border: 5px solid transparent;
    text-align: center;
    text-decoration: none;
   }
 
   a.portfolio:hover h2 {
    text-decoration: underline;
   }

   a.portfolio h2 {
    margin: 0px;
   }

   a.portfolio p {
    margin: 5px;
    font-size: small;
   }

   img {
    border: 0px;
    width: 250px;
    height: 200px;
   }
  </style>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28793424-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

  </script>
 </head>
 <body>
  <h1>Portfolio of <span>Chris Smith</span></h1>

  <p>
   This is a portfolio of things which I've worked on, or made, in my spare
   time. It includes both large and small projects (such as DMDirc and the
   collection of G15Control plugins, respectively), but only those that are
   reasonably complete, and that I've worked on recently.
  </p>

  <a href="/projects/dmdirc" class="portfolio">
   <img src="images/dmdirc.png" alt="DMDirc screenshot">
   <h2>DMDirc</h2>
   <p>An open-source Java IRC client, developed with
      Shane Mc Cormack and Gregory Holmes</p>
  </a>

  <a href="/projects/japoker" class="portfolio">
   <img src="images/japoker.png" alt="JaPoker screenshot">
   <h2>JaPoker</h2>
   <p>A Java poker game with computer controlled
      opponents. Supports many different poker variations.</p>
  </a>

  <a href="/projects/tf2stats" class="portfolio">
   <img src="images/tf2stats.png" alt="TF2 Stats screenshot">
   <h2>TF2 Stats</h2>
   <p>Log analyser and statistics generator for Team Fortress 2 game servers.</p>
  </a>

  <a href="/projects/utd" class="portfolio">
   <img src="images/utd.png" alt="UTD-Hosting control panel">
   <h2>UTD-Hosting</h2>
   <p>Small web hosting company with custom built control panel software
      for customers (written in PHP).</p>
  </a>

  <a href="/projects/dmdirc-addons" class="portfolio">
   <img src="images/dmdirc-addons.png" alt="DMDirc addons directory">
   <h2>Addons Directory</h2>
   <p>Website to allow third-part developers of DMDirc addons
      to share them with users.</p>
  </a>

  <a href="/projects/g15" class="portfolio">
   <img src="images/g15.png" alt="G15Control plugins">
   <h2>G15Control Plugins</h2>
   <p>A collection of plugins for G15Control that display system/app information.</p>
  </a> 

  <a href="/projects/openid" class="portfolio">
   <img src="images/openid.png" alt="PHP OpenID Consumer">
   <h2>Poidsy</h2>
   <p>A PHP OpenID consumer that's easy for developers to use.</p>
  </a>

  <a href="/projects/jircstats" class="portfolio">
   <img src="images/jircstats.png" alt="JIRCStats">
   <h2>JIRCStats</h2>
   <p>A Java IRC log analyser and statistics generator.</p>
  </a>

  <a href="/projects/conquest" class="portfolio">
   <img src="images/conquest.png" alt="Conquest">
   <h2>Conquest</h2>
   <p>"The strategy game of dice", prize-winning university group project.</p>
  </a>

  <div id="footer"></div>
  &laquo; Back to <a href="/">homepage &amp; contact information</a>
 </body>
</html>
