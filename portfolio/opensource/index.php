<?PHP

 if (!preg_match('/chris\.smith/i', $_SERVER['HTTP_HOST'])) {
  header('Location: http://chris.smith.name/portfolio/opensource/');
  exit();
 }


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <title>Portfolio of Chris Smith</title>
   <style type="text/css">
   body {
    margin: 50px 10%;
   }

   h1 {
    border-bottom: 3px double #aaa;
    color: #aaa;
   }

   div#footer {
    clear: both;
    padding-top: 20px;
    border-bottom: 3px double #aaa;
    margin-bottom: 6px;
   }

   h1 span {
    padding-left: 3px;
    color: #000;
   }

   p {
    line-height: 150%;
   }

   a.portfolio {
    display: block;
    float: left;
    margin: 5px 10px;
    min-height: 300px;
    width: 300px;
    height: 300px;
    color: black;
    border: 5px solid transparent;
    text-align: center;
    text-decoration: none;
   }
 
   a.portfolio:hover h2 {
    text-decoration: underline;
   }

   a.portfolio h2 {
    margin: 0px;
   }

   a.portfolio p {
    margin: 5px;
    font-size: small;
   }

   img {
    border: 0px;
    width: 250px;
    height: 200px;
   }
  </style>
 </head>
 <body>
  <h1>Portfolio of <span>Chris Smith</span></h1>

  <p>
   This section of the portfolio lists contributions I've made to
   open source projects, either as part of a job or in my spare time.
   It excludes any open source projects that I started myself, which are listed
   on the main <a href="/portfolio">portfolio page</a>. It also only
   lists those contributions that were accepted and included in a release.
  </p>

  <h2>Hadoop</h2>
  <ul>
   <li><a href="https://issues.apache.org/jira/browse/HADOOP-372">[HADOOP-0372] Ability to specify different InputFormat classes for different input dirs for Map/Reduce jobs</a></li>
   <li><a href="https://issues.apache.org/jira/browse/HADOOP-3714">[HADOOP-3714] Bash tab completion support</a></li>
   <li><a href="https://issues.apache.org/jira/browse/HADOOP-3791">[HADOOP-3791] Use generics in ReflectionUtils</a></li>
  </ul>

  <div id="footer"></div>
  &laquo; Back to <a href="/">homepage &amp; contact information</a> or <a href="/portfolio">my portfolio</a>
 </body>
</html>
