<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:owl="http://www.w3.org/2002/07/owl#"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="xml" indent="yes"/>
 <xsl:template match="/">
  <html>
   <head>
    <title>Personal homepage of Chris Smith</title>
    <link rel="openid.server" href="http://www.myopenid.com/server"/>
    <link rel="me openid.delegate" href="http://chris87.myopenid.com/"/>
    <link rel="me meta" type="application/rdf+xml" title="FOAF" href="http://chris.smith.name/foaf.xml"/>
    <link rel="stylesheet" type="text/css" href="/style.css"/>
    <xsl:for-each select="//foaf:holdsAccount/foaf:OnlineAccount">
     <link rel="me">
      <xsl:attribute name="title">
       <xsl:value-of select="rdfs:label"/>
      </xsl:attribute>

      <xsl:attribute name="href">
       <xsl:value-of select="@rdf:about"/>
      </xsl:attribute>
     </link>
    </xsl:for-each>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28793424-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

    </script>
   </head>
   <body>
    <h1>Personal homepage of <span>Chris Smith</span></h1>
    <div class="left">
     <h2>Me</h2>
     <p>
      I'm a Linux-using, open-source-advocating software developer. I'm interested in application, computer and network security, data mining, and usability.
     </p>
     <p>
      Development-wise, my current language of choice is Java, but I also widely use PHP.  I can use HTML, CSS, XML, JavaScript, and SQL.  I used to use (and probably still could if needed) Delphi (object pascal), Visual Basic and Tcl. I have a basic knowledge of C, Ruby, Python and Perl.
     </p>
    </div>
    <div class="right">

     <h2>Projects</h2>
     <p>
      Some of the recent projects I've been working on include:
     </p>
     <dl>
      <dt><a href="/projects/poidsy">Poidsy</a></dt>
      <dd>A PHP OpenID consumer</dd>
      <dt><a href="/projects/dmdirc">DMDirc</a></dt>
      <dd>An open-source, cross-platform IRC client</dd>
     </dl>
     <p>
      A complete list of projects is available on my
      <a href="/portfolio/">portfolio</a> page.
     </p>
    </div>
    <div class="left">

     <h2>This site</h2>
     <p>
      There used to be a reasonably large site here, instead of this one page.  I rarely updated it, and I can't imagine anyone was that interested in the content, so instead of having a site that I felt guilty for not updating, I scrapped it and simplified the content down to this page.
     </p>
    </div>
    <div class="right">
     <h2>Contact</h2>
     <p>

      I can be contacted by e-mail at <a href="mailto:chris87@gmail.com">chris87@gmail.com</a> (I get so much spam it's not worth me trying to obfuscate it).  Alternatively, you can contact me on <a href="irc://irc.quakenet.org/mdbot">irc</a> (my nickname is 'MD87'), Google Talk or MSN (both using my e-mail address above).
     </p>
    </div>
    <div id="footer"></div>
   </body>
  </html>
 </xsl:template>
</xsl:stylesheet>
